# __author__ = 'Pawel Polit'

import ip

with open('txt_files/traceroute.txt', 'r') as traceroute:
    with open('txt_files/traceroute_processed.txt', 'w') as out:
        my_ip = ip.get_my_ip()
        previous_ip = ''
        for line in traceroute:
            line_table = line.split()
            if len(line_table) == 3:
                if line_table[1] != previous_ip:
                    previous_ip = line_table[1]
                    out.write(previous_ip + '\n')
            elif len(line_table) == 1:
                previous_ip = ''
                out.write('\n' + my_ip + '\n')
