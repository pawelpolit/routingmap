# __author__ = 'Pawel Polit'

import csv
import numpy


def fill_ip_with_zeros(ip):
    ip = ip.split('.')

    for i in range(len(ip)):
        ip[i] = (3 - len(ip[i])) * '0' + ip[i]

    ip = ".".join(ip)
    return ip


def ip_to_coordinates__ip_pair_to_frequency():
    with open('data_base/GeoLite2-City-Blocks-IPv4.csv', 'r') as csv_file:
        database = csv.reader(csv_file)
        database.next()

        coordinates = []
        ip_list = []
        for row in database:
            if len(row[-2]) > 0 and len(row[-1]) > 0:
                coordinates.append([float(row[-2]), float(row[-1])])
                ip = fill_ip_with_zeros(row[0][:row[0].index('/')])
                ip_list.append(ip)

    with open('txt_files/traceroute_processed.txt', 'r') as traceroute:
        ip_list = numpy.array(ip_list)
        ip_to_coordinates = {}
        ip_pair_to_frequency = {}

        traceroute_ips = traceroute.read().split('\n')

        for i in range(1, len(traceroute_ips)):
            if len(traceroute_ips[i - 1]) == 0 and len(traceroute_ips[i]) > 0:
                ip = fill_ip_with_zeros(traceroute_ips[i])

                if ip not in ip_to_coordinates:
                    ip_to_coordinates[ip] = coordinates[numpy.searchsorted(ip_list, ip)]
            elif len(traceroute_ips[i - 1]) > 0 and len(traceroute_ips[i]) > 0:
                first_ip = fill_ip_with_zeros(traceroute_ips[i - 1])
                second_ip = fill_ip_with_zeros(traceroute_ips[i])

                if second_ip not in ip_to_coordinates:
                    ip_to_coordinates[second_ip] = coordinates[numpy.searchsorted(ip_list, second_ip)]

                key = first_ip + ' ' + second_ip

                if key not in ip_pair_to_frequency:
                    ip_pair_to_frequency[key] = 1
                else:
                    ip_pair_to_frequency[key] += 1

    return [ip_to_coordinates, ip_pair_to_frequency]
