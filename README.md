# README #

Instruction:
1. Create folders: data_base and txt_files
2. Download GeoLite2 using link: http://geolite.maxmind.com/download/geoip/database/GeoLite2-City-CSV.zip
3. Copy GeoLite2-City-Blocks-IPv4.csv to folder data_base
4. Run tshark and save result in txt_files/tshark.txt
5. Run tshark_preprocessing.py
6. Run prepare_traceroute.py
7. Run traceroute_preprocessing.py
8. Run draw_map.py