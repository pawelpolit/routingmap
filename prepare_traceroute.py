# __author__ = 'Pawel Polit'

from scapy.all import *
import sys


with open('txt_files/tshark_processed.txt', 'r') as tshark:
    number_of_lines = float(len(tshark.read().split('\n')))

with open('txt_files/tshark_processed.txt', 'r') as tshark:
    with open('txt_files/traceroute.txt', 'w') as traceroute_file:
        std_out = sys.stdout
        line_number = 1

        for line in tshark:
            ip = line[:-1]
            sys.stdout = std_out
            result, unans = traceroute(ip, maxttl=32)

            print '\n'
            print str(round(line_number / number_of_lines * 100, 2)) + ' %'
            print '\n'
            line_number += 1

            sys.stdout = traceroute_file
            result.show()
            print ''
