# __author__ = 'Pawel Polit'

import regex

with open('txt_files/tshark.txt', 'r') as tshark:
    with open('txt_files/tshark_processed.txt', 'w') as out:
        for line in tshark:
            line_table = line.split()
            ip = line_table[4]
            if regex.match('^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$', ip) is not None:
                out.write(ip + '\n')
