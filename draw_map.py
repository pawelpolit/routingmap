# __author__ = 'Pawel Polit'

import mpl_toolkits

mpl_toolkits.__path__.insert(0, '/usr/lib/python2.7/dist-packages/mpl_toolkits')
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import mapping

latitude_min, latitude_max = -90.0, 90.0
longitude_min, longitude_max = -180.0, 180.0

fig = plt.figure(figsize=(20.0, 10.0))
plt.subplots_adjust(left=0.125, right=0.9, top=0.9, bottom=0.1, wspace=0.2, hspace=0.2)

m = Basemap(resolution='c', projection='cyl', llcrnrlat=latitude_min, urcrnrlat=latitude_max, llcrnrlon=longitude_min,
            urcrnrlon=longitude_max)

m.drawcountries(linewidth=0.5)
m.drawcoastlines(linewidth=0.5)
m.fillcontinents()

m.drawparallels(np.arange(latitude_min, latitude_max, 10.), labels=[1, 0, 0, 0], color='black', dashes=[1, 5],
                labelstyle='+/-', linewidth=0.2)
m.drawmeridians(np.arange(longitude_min, longitude_max, 15.), labels=[0, 0, 0, 1], color='black', dashes=[1, 5],
                labelstyle='+/-', linewidth=0.2)

ip_to_coordinates, ip_pair_to_frequency = mapping.ip_to_coordinates__ip_pair_to_frequency()

frequencies = ip_pair_to_frequency.values()
max_frequency = float(max(frequencies))
min_frequency = float(min(frequencies))

for coordinates in ip_to_coordinates.values():
    longitude, latitude = m(coordinates[1], coordinates[0])
    m.plot(longitude, latitude, 'bo', markersize=10)

for ip_pair in ip_pair_to_frequency:
    first_ip, second_ip = ip_pair.split()

    first_longitude, first_latitude = m(ip_to_coordinates[first_ip][1], ip_to_coordinates[first_ip][0])
    second_longitude, second_latitude = m(ip_to_coordinates[second_ip][1], ip_to_coordinates[second_ip][0])

    width = (ip_pair_to_frequency[ip_pair] - min_frequency) / (max_frequency - min_frequency) * 5 + 0.1

    m.drawgreatcircle(first_longitude, first_latitude, second_longitude, second_latitude, linewidth=width, color='r')

plt.title("Routing map")
plt.show()
